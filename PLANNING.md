# Planning

|      | Date       | Focus                                                        |
| ---- | ---------- | ------------------------------------------------------------ |
| 1    | 17.02.2020 | Introduce **objectives** and **guidelines**. <br />Present and experiment with **rapid cycle workflow**. <br />Create **groups** and **ideate**. Describe and assign **roles** within the teams.<br />At the end of the session, **every group must be able to present the project idea**. Every team member must have used the rapid workflow to write a document where he/she describes his perspective on the project idea. <br />**Every team has created a Telegram group and invited me** (if you want to use another comm. tool, let me kknow) |
| 2    | 24.02.2020 | **Every team presents the project idea and the role of every team member**<br />Define the **long term** vision and the **MVP** scope (the goal is to deliver the MVP at the end of the semester). <br />Specify the **scenario** that will be used to **demo the product** at the end of the semester. <br />Create a **landing page** to present the product idea. |
| 3    | 02.03.2020 | **Refine the demo scenario** and create **mockups** for the UI/UX.<br />Decide what **interface**(s) (desktop only? desktop + mobile?) will be implemented. |
| 4    | 09.03.2019 | **First presentation**. The person responsible for the UI/UX presents the product idea and UI mockups in **10 minutes**. <br />The landing page is up and running. The GitLab repo is clean. <br />**Architecture and planning for the first development iteration**. Pick the **technology stack** (what will be used in the backend, in the frontend and to implement the build pipeline). For instance: Springboot in the backend, Vue.js in the front-end and GitLab pipelines for the CI. **Pick a simple functional slice of the system to be implemented in 4 weeks**. |
| 5    | 16.03.2020 | Development                                                  |
| 6    | 23.03.2020 | Development                                                  |
| 7    | 30.03.2020 | Development                                                  |
| 8    | 06.04.2020 | Development                                                  |
|      | 13.04.2020 | Eastern holidays                                             |
| 9    | 20.04.2020 | **Second presentation**. In 10 minutes, the person in charge of DevOps presents the structure of the GitLab repo, explains how the back-end and front-end is automatically built and executed locally. The person also explains which automated tests have been created by the back-end and front-end developers and how they are run in the pipeline. |
| 10   | 27.04.2020 | Development                                                  |
| 11   | 04.05.2020 | Development                                                  |
| 12   | 11.05.2020 | Development                                                  |
| 13   | 18.05.2020 | Development **[CODE FREEZE on 24.05.2020]** - repos and code will be evaluated after that date |
| 14   | 25.05.2020 | Preparation of the final demo and documentation              |
|      | 01.06.2020 | (Pentecôte) Preparation of the final demo and documentation  |
| 15   | 08.06.2020 | **Final presentations**. Time to shine!                      |

## Workload

"Périodes encadrées: 16 x 4 = 64 périodes, soit 48 heures."

"Charge de travail totale: 90 heures (soit 42 heures en plus des périodes encadrées)."

90 hours ~= 2 weeks of work full time

* I have 2 weeks to implement a back-end, where I expose a domain model via a REST API. I need time to design the model, the API. I need time to write automated tests.
* I have 2 weeks to implement the front-end for my application (mobile and/or desktop). I need to create atomic elements, which I can assemble in larger components and finally in pages. I need to manage the navigation between pages. I need to decide what kind of tests will make the development process efficient.
* I have 2 weeks to design and implement two runtime environments: one local and one in the cloud. I have to implement a CI/CD pipeline to automatically build and test the software, before releasing it into the cloud.
* I have 2 weeks to help my team mates stay deliver what is required to run the demo that will convey the vision of our product. Depending on the situation, I might spend time working in the back-end, the front-end or the runtime environments. But I need to maintain a global view on the progress of the project.

