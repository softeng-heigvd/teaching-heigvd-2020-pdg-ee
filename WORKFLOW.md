# The Rapid Cycle Workflow

**Agile methods and practices put a strong emphasis on the flow of activities that need to be done when developing a software product**.

At the **macro-level**, one objective is to minimize the time needed to more from an idea to a feature released and available to users. This is what is called the "**lead time**". Many factors influence the lead time. 

Some factors are **organizational**. Are people working in silos such as a "product organization", a "development organization", a "quality assurance" organization, an "IT operations" organization or are they organized in small, multi-disciplinary and autonomous teams? In the first scenario, there is likely to be a lot of friction and queuing, which will cause the lead time to skyrocket. In the second scenario, teams are usually able to move faster because they control (and are responsible) the entire value chain.

Other factors are **technical**. When software is aging, there is a risk to see an increase of so called "**technical debt**". Technical debt is the result of shortcuts and quick hacks that are sometimes done to ship code quickly. In the short term, the team is borrowing some time (to be able to meet a deadline, do a demo). But this does not come for free. Over time, the messy codebase, the lack of automated tests, the absence of documentation will make the team slower and slower. For every new feature, developers will have to pay an interest on the time they have initially borrowed. This is why it is important to sometimes pay back the technical debt, for instance by refactoring and cleaning up code.

At the **micro-level**, one objective is to make individual developers more productive by offering them a way to organize their time and to track their progress. **Having a regular rhythm and having a measure of progress puts you in the flow, making software development highly enjoyable**. ¨

This document, and what we have called the "rapid cycle workflow" for the lack of a better name, focuses on this micro-level dimension. Our goal is to give you a simple tool that should allow to better organize your activities and manage your time. As a side effect, it will enable a visualization of the work done during the project.

## Show me the workflow!

Enough said, let's see the workflow in action, step by step.

1. **Think about what you intend to achieve**. This should be something quite small. Something that you should be able to finish in the current work session (which could be a couple of hours long). Do not pick something like "Implement a chat system for my multi-player game". Pick something like "Design the REST API of the chat service" or "Implement the /rooms endpoint of the REST API".
2. **Define the tangible artefacts that you commit to deliver as part of the activity**. Often, you will produce code. But sometimes, you will deliver documentation (text or diagrams). Every single activity must have a deliverable. For instance, if you define an exploration activity such as "Search for potential Javascript libraries", make sure to at least write a markdown document in the repo.
3. Move to GitLab, **create an issue** and document your objectives. Take the time to fill out the fields with informative information. It will help you, your team mates and will become part of the project memory and documentation.
4. ~~In the issue description, **record your estimation of the time** that you will need to complete the activity. You can do this with the `/estimate 3h` quick action, as explained in [GitLab documentation](https://gitlab.com/help/workflow/time_tracking.md). If you estimate in days and not hours, it is a sign that you are working with issues that are too big.~~
5. Before doing any further work, **create a merge request**. It will be labeled by GitLab as a Work In Progress (**WIP**) merge request. This enables a very nice collaborative work style: the ongoing activities of team members are always and immediately visible. This makes it possible to stay aware of what others are doing. This also makes it possible to give feedback by adding comments on the merge request.
6. In the **merge request** add a **comment** to **record your estimation of the time** that you will need to complete the activity. You can do this with the `/estimate 3h` quick action, as explained in [GitLab documentation](https://gitlab.com/help/workflow/time_tracking.md). If you estimate in days and not hours, it is a sign that you are working with issues that are too big.~~
7. **Checkout the branch** in your local repo (after doing a `git fetch`).
8. Do the work. Do it in **very small increments**: push commits and add comments very often and not just at the end. Try it and you will see that it will help you "**get into the zone**".
9. **Whenever you complete a small task related to the current merge request (or on a regular interval)**, log your work in a comment with the `/spend 20m`. GitLab will sum up all these work logs, which will make it possible to answer various questions during and at the end of the project: are we good at estimating tasks? where did we spend most of the time, how much time did we spend on writing tests vs writing production code? You see that if you organize your work in small tasks (hence issues and merge requests), it is easier to get insights about the development process later on. **Do not wait the end of the task, or worse the end of the project to record time spent. You will not be able to remember and the data will have to be discarded**.
10. When you are done, **commit all changes on your code or documentation artifacts**. You can then change the state of your merge request by clicking on the "**resolve WIP status**" button.
11. **Finally, you can accept the merge request** (we will keep things short here and not discuss what you need to do in terms of testing and validation).

## Diagram

![workflow](./diagrams/workflow.png)